class App {
String appName;
String appSector;
String appDeveloper;
int yearWon;

App(this.appName, this.appSector, this.appDeveloper, this.yearWon);

void describe() {
print("AppName: $appName\nSector :$appSector \nDeveloper: $appDeveloper\nYear:$yearWon");
 }

void toCapital() {
 print(appName.toUpperCase()); 
 }
}


void main() {
  App myapp =
      App("Shyft", "Finance Solution", "Brett Patrontasch", 2017);

myapp.describe ();
myapp.toCapital();
}
